using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(NavMeshAgent))]
public class PlayerController : MonoBehaviour
{
    public float stopDistance = 0.1f;
    public AudioClip Pet;
    public AudioClip Dead;
    public AudioClip Calling;
    public AudioClip Jump;
    private AudioSource audioSource;

    private Camera mainCamera;
    private NavMeshAgent agent;
    private Animator animator;

    private void Start()
    {
        mainCamera = Camera.main;
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        // Check for player input
        if (Input.touchCount > 0)
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }
            // Raycast to the touch position
            Ray ray = mainCamera.ScreenPointToRay(Input.GetTouch(0).position);
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
                {
                    return;
                }
                // Move the player to the touch position
                audioSource.clip = Calling;
                audioSource.Play();
                agent.SetDestination(hit.point);

                // Stop the player if it gets close to the camera
                if (Vector3.Distance(transform.position, mainCamera.transform.position) <= stopDistance)
                    agent.isStopped = true;

                else
                    agent.isStopped = false;

            }
        }
    }

    public void CallPet()
    {
        agent.SetDestination(mainCamera.transform.position);
        audioSource.clip = Calling;
        audioSource.Play();
    }

    public void Petting()
    {
        animator.Play("CharacterArmature_Dance");
        animator.Play("CharacterArmature_Dance");
        animator.Play("CharacterArmature_Dance");
        animator.Play("CharacterArmature_Dance");
        animator.Play("CharacterArmature_Dance");
        audioSource.clip = Pet;
        audioSource.Play();
    }

    public void PlayDead()
    {
        animator.Play("CharacterArmature_Death");
        audioSource.clip = Dead;
        audioSource.Play();
    }

    public void Jumping()
    {
        animator.Play("CharacterArmature_Jump");
        animator.Play("CharacterArmature_Jump");
        audioSource.clip = Jump;
        audioSource.Play();

    }
}
