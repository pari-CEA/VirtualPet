using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SpawnManager : MonoBehaviour
{
    public GameObject playerPrefab;
    public UiController uiController;

    private void SpawnPlayer()
    {
        GameObject playerObject = Instantiate(playerPrefab, Vector3.zero, Quaternion.identity);
        PlayerController playerController = playerObject.GetComponent<PlayerController>();
        uiController.SetPlayerController(playerController);
    }

}


