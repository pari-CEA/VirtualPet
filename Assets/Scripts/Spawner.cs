using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.AI;
using Unity.AI.Navigation;
//Didn't work out Had no idea what was going wrong 
public class Spawner : MonoBehaviour
{
    public GameObject planePrefab;
    public GameObject petPrefab;
    public GameObject UIcanvas;
    private bool planeSpawned = false;
    private bool petSpawned = false;
    private ARRaycastManager raycastManager;
    private ARPlaneManager planeManager;
    private ARPointCloudManager pointCloudManager;
    private NavMeshSurface navMeshSurface;
    private UiController uiController;


    private void Awake()
    {
        raycastManager = GetComponent<ARRaycastManager>();
        planeManager = GetComponent<ARPlaneManager>();
        pointCloudManager = GetComponent<ARPointCloudManager>();
        navMeshSurface = GetComponent<NavMeshSurface>();
    }

    private void Update()
    {
        if (!planeSpawned)
        {
            SpawnPlane();
        }
        else if (!petSpawned)
        {
            SpawnPet();
        }

    }

    private void SpawnPlane()
    {
        // Check if we hit a plane
        Vector2 screenCenter = new Vector2(Screen.width / 2f, Screen.height / 2f);
        List<ARRaycastHit> hits = new List<ARRaycastHit>();
        if (raycastManager.Raycast(screenCenter, hits, TrackableType.PlaneWithinPolygon))
        {
            // Spawn the plane prefab
            GameObject plane = Instantiate(planePrefab, hits[0].pose.position, hits[0].pose.rotation);

            // Set the planeSpawned flag to true so we only spawn the plane once
            planeSpawned = true;

            // Disable the AR plane and point cloud managers
            planeManager.enabled = false;
            pointCloudManager.enabled = false;
            planeManager.enabled = false;

            // Bake navmesh on the plane
            navMeshSurface.BuildNavMesh();
        }
    }

    private void SpawnPet()
    {
        // Check if the user taps the screen
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            // Raycast to detect if the user tapped on the plane
            Vector2 touchPosition = Input.GetTouch(0).position;
            List<ARRaycastHit> hits = new List<ARRaycastHit>();
            if (raycastManager.Raycast(touchPosition, hits, TrackableType.PlaneWithinPolygon))
            {
                // Spawn the pet prefab at the hit position
                GameObject pet = Instantiate(petPrefab, hits[0].pose.position, Quaternion.identity);

                // Set the petSpawned flag to true so we only spawn the pet once
                petSpawned = true;

                // Set the pet's rotation to face the camera only on the Y axis
                Vector3 cameraPosition = Camera.main.transform.position;
                cameraPosition.y = pet.transform.position.y;
                pet.transform.LookAt(cameraPosition);
                Instantiate(UIcanvas);

            }
        }
    }

}

