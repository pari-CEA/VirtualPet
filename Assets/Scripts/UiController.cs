using UnityEngine;


public class UiController : MonoBehaviour
{
    private PlayerController _player;

    public void SetPlayerController(PlayerController controller)
    {
        _player = controller;
    }
    void Start()
    {
        // Find the player GameObject by tag and get a reference to its PlayerController script
        GameObject playerObject = GameObject.FindGameObjectWithTag("Player");
        if (playerObject != null)
        {
            _player = playerObject.GetComponent<PlayerController>();
        }
    }


    public void Call()
    {
        if (_player != null)
        {
            _player.CallPet();
        }
    }

    public void Dead()
    {
        _player.PlayDead();
        Debug.Log("Dead");
    }
    public void Jump()
    {
        if (_player != null)
        {
            _player.Jumping();
        }
    }

    public void Pet()
    {
        _player.Petting();
        Debug.Log("Pet");
    }
}
