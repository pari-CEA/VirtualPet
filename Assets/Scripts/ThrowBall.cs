using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine. XR. ARFoundation;

public class ThrowBall : MonoBehaviour
{


    [SerializeField]
    ARRaycastManager m_RaycastManager;

    [SerializeField]
    private GameObject ball;

    List<ARRaycastHit> m_Hits = new List<ARRaycastHit>();


    void Update()
    {
        if (Input.touchCount == 0)
            return;
        if (m_RaycastManager.Raycast(Input.GetTouch(0).position, m_Hits))
        {
            // Only returns true if there is at least one hit
            foreach (var hit in m_Hits)
            { HandleRaycast(hit); }
        }
    }

        void HandleRaycast(ARRaycastHit hit)
        {
            if (hit.trackable is ARPlane plane)
            {
                // Do something with 'plane':
                Debug.Log($"Hit a plane with alignment {plane.alignment}");
                Instantiate(ball, hit.pose.position, Quaternion.identity);
            }
            else
            {   // What type of thing did we hit?
                Debug.Log($"Raycast hit a {hit.hitType}");
            }
        }

    
}