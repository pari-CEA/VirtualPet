using UnityEngine;
using UnityEngine.AI;

public class PlayerSpawner : MonoBehaviour
{
    public GameObject playerPrefab;
    public Camera mainCamera;
    public GameObject UIcanvas;

    private bool playerSpawned = false;
    private UiController uiController;

    void Update()
    {
        if (!playerSpawned)
        {
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                Ray ray = mainCamera.ScreenPointToRay(Input.GetTouch(0).position);
                RaycastHit hit;

                // Cast a ray from the camera to the ground
                if (Physics.Raycast(ray, out hit))
                {
                    // Check if the hit object is on the NavMesh
                    NavMeshHit navMeshHit;
                    if (NavMesh.SamplePosition(hit.point, out navMeshHit, 0.1f, NavMesh.AllAreas))
                    {
                        // Spawn the player at the hit point and rotate towards the camera
                        Vector3 spawnPoint = navMeshHit.position;
                        Quaternion rotation = Quaternion.LookRotation(mainCamera.transform.forward, Vector3.up);
                        Instantiate(playerPrefab, spawnPoint, rotation);

                        Instantiate(UIcanvas);
                        playerSpawned = true;
                    }
                }
            }
        }
    }
}
